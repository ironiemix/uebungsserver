# Übungsserver für Datenbankunterricht

## TL;DR: Inbetrebnahme mit Debian 10 (buster) oder Debian 11 (bullseye)

```
apt update
apt install git ansible vim gpg
git clone https://codeberg.org/ironiemix/uebungsserver.git
cd uebungsserver 
./manage --install
```

[![asciicast](https://asciinema.org/a/4sEwVuiO8Cmxd4mKl5DsSIuS4.svg)](https://asciinema.org/a/4sEwVuiO8Cmxd4mKl5DsSIuS4)