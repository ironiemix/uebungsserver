#!/bin/bash

# Execute ansible playbooks to create a
# "uebungsserver" environment for
# learning mysql an php
#
# GPL v3
#
# Frank Schiebel <frank@talheim.net>

if [ ! -f prepare.yml ]; then 
	echo "------------------------------------------"
	echo "Dieses Skript muss im Basisverzeichnis des"
	echo "Git-Repos aufgerufen werden."
	echo "[Fehler: prepare.yml nicht gefunden]"
	echo 
	echo "./manage"
	echo "------------------------------------------"
	exit 0
fi

SETTINGS="settings.yml"

if [ ! -f $SETTINGS ]; then 
	echo "------------------------------------------"
	echo "Keine settings.yml gefunden."
	echo -n "Erzeuge Vorlage..."
	cat > $SETTINGS <<-EOSET
		# Die Benutzerpasswörter werden jeweils auf 
		# den Benutzernamen + den Wert gesetzt, der 
		# hier angegeben wird.
		#
		# Benutzer: dbuser05
		# Passwort: dbuser05CHANGEME
		#
		# Shell-Benutzer: shellu05
		# Passwort: shellu05!CHANGEME
		#
		mysql_userpart_password: "CHANGEME"

		# Wenn der Server unter einem DNS Namen erreichbar ist
		# kann man diesen hier eintragen, dann wird automatisch ein 
		# SSL Zertifikat geholt und installiert
		# z.B.:
		# Für den internen Raspi "" stehen lassen
		#
		# configure_ssl_domain: "db.my.domain"
		configure_ssl_domain: "SSLNAME"
		
EOSET
	pw=$(openssl rand -base64 24)
	echo "mysql_root_password: \"$pw\"" >> $SETTINGS
	pw=$(openssl rand -base64 24)
	echo "mysql_phpmyadmin_password: \"$pw\"" >> $SETTINGS
	echo 
	echo "Bitte geben Sie das Passwort-Suffix an, das"
	echo "an die Benutzernamen angehängt wird, um halbwegs"
	echo "sichere Kennwörter zu erzeugen"
	echo -n "Suffix: "
        read suffix
	sed -i "s/CHANGEME/$suffix/g" $SETTINGS	

	echo 
	echo "Bitte geben Sie den Hostnamen mit Domain (FQDN) an"
	echo "wenn Sie ein SSL Zertiofikat erhalten wollen."
	echo "Andernfalls lassen Sie die Eingabe leer und drücken"
	echo "Sie Enter."
	echo -n "SSL Hostnane (FQDN): "
        read sslname
	sed -i "s/SSLNAME/$sslname/g" $SETTINGS	
	echo " done."
fi

suffix=$(grep mysql_userpart_password: settings.yml  | cut -d: -f2 | sed s/\"//g | xargs)
sslname=$(grep -v ^# settings.yml | grep configure_ssl_domain:   | cut -d: -f2 | sed s/\"//g | xargs)

echo "-------------------------------------------------------------"
echo "Der Übungsserver hat folgende Einstellungen:"
echo " Datenbank-Benutzer: dbuser<NUM>"
echo " Datenbank-Passwort: dbuser<NUM>$suffix"
echo 
echo " Shell-Benutzer: shellu<NUM>"
echo " Shell-Passwort: shellu<NUM>!$suffix"
echo 
echo " <NUM> ist 01,02,03,...,50"
if [ "x$sslname" != "x" ]; then 
echo 
echo "Der Übungsserver soll unter"
echo "   https://$sslname/  "
echo "erreichbar sein"
fi 
echo "-------------------------------------------------------------"

if [ "x$1" != "x--install" ]; then 
	exit 0
fi 

# getting submodules
git submodule init
git submodule update

ansible-playbook -i "localhost," prepare.yml
ansible-playbook -i "localhost," configure.yml
